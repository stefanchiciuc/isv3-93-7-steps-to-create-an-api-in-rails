# 7 Steps to Create an API in Rails

## Overview

This API allows you to perform CRUD operations for a hiking trail database. The database contains information about forests, trails and hikers. The API is built using Ruby on Rails.

## Getting Started

Follow these steps to set up and run the API:

1. Clone this repository to your local machine.

2. Run Docker Compose to start the containers.
```bash
docker-compose up -d
```

3. Create the database.
```bash
docker-compose exec app rails db:create
```

4. Run the migrations.
```bash
docker-compose exec app rails db:migrate
```

5. Seed the database.
```bash
docker-compose exec app rails db:seed
```

6. Import the ISV3-93.postman_collection.json file in Postman to test and use the API.

#### API Endpoints:
---
###### * GET /forests: Get a list of all forests.
###### * GET /forests/:id: Get details of a specific forest.
###### * POST /forests: Add a new forest.
###### * PUT /forests/:id: Update an existing forest .
###### * DELETE /forests/:id: Remove a forest.
***
###### * GET /trails: Get a list of all trails.
###### * GET /trails/:id: Get details of a specific trail.
###### * POST /trails: Add a new trail.
###### * PUT /trails/:id: Update an existing trail.
###### * DELETE /trails/:id: Remove a trail.
***
###### * GET /hikers: Get a list of all hikers.
###### * GET /hikers/:id: Get details of a specific hiker.
###### * POST /hikers: Add a new hiker.
###### * PUT /hikers/:id: Update an existing hiker.
###### * DELETE /hikers/:id: Remove a hiker.
