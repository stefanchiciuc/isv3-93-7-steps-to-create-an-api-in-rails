Hiker.destroy_all
Trail.destroy_all
Forest.destroy_all

forest1 = Forest.create(name: 'Tongass National Forest', state: 'Alaska')
forest2 = Forest.create(name: 'Chugach National Forest', state: 'Colorado')
forest3 = Forest.create(name: 'Humboldt-Toiyabe National Forest', state: 'Nevada')
forest4 = Forest.create(name: 'Bridger-Teton National Forest', state: 'Wyoming')
forest5 = Forest.create(name: 'Tonto National Forest', state: 'Arizona')

trail1 = Trail.create(name: 'Tongass National Forest Trail', miles: 4, forest: forest1)
trail2 = Trail.create(name: 'Chugach National Forest Trail', miles: 5, forest: forest2)
trail3 = Trail.create(name: 'Humboldt-Toiyabe National Forest Trail', miles: 6, forest: forest3)
trail4 = Trail.create(name: 'Bridger-Teton National Forest Trail', miles: 7, forest: forest4)
trail5 = Trail.create(name: 'Tonto National Forest Trail', miles: 8, forest: forest5)

Hiker.create(name: 'John Doe', age: 25, trail: trail1)
Hiker.create(name: 'Jane Smith', age: 30, trail: trail2)
Hiker.create(name: 'John Smith', age: 35, trail: trail3)
Hiker.create(name: 'Jane Doe', age: 40, trail: trail4)
Hiker.create(name: 'John Doe', age: 45, trail: trail5)
Hiker.create(name: 'Jane Smith', age: 50, trail: trail1)
