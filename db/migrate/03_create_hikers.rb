require_relative '02_create_trails'

class CreateHikers < ActiveRecord::Migration[7.0]
  def change
    create_table :hikers do |t|
      t.string :name
      t.integer :age
      t.references :trail, null: false, foreign_key: true

      t.timestamps
    end
  end
end
