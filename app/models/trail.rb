class Trail < ApplicationRecord
  has_many :hikers
  belongs_to :forest
end
