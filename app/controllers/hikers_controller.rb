class HikersController < ApplicationController
  before_action :set_hiker, only: %i[show update destroy]

  def index
    @hikers = Hiker.all
    render json: @hikers
  end

  def show
    render json: @hiker
  end

  def create
    @hiker = Hiker.new(hiker_params)

    if @hiker.save
      render json: @hiker, status: :created
    else
      render json: @hiker.errors, status: :unprocessable_entity
    end
  end

  def update
    if @hiker.update(hiker_params)
      render json: @hiker
    else
      render json: @hiker.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @hiker.destroy
    render json: { message: 'Hiker deleted successfully' }
  end

  private

  def set_hiker
    @hiker = Hiker.find(params[:id])
  end

  def hiker_params
    params.require(:hiker).permit(:name, :age, :trail_id)
  end
end
