class ForestsController < ApplicationController
  def index
    @forests = Forest.all

    render json: @forests
  end

  def show
    @forest = find_forest_by_id

    render json: @forest
  end

  def create
    @forest = Forest.create(name: params[:name], state: params[:state])

    render json: @forest
  end

  def update
    @forest = find_forest_by_id
    @forest.update(name: params[:name], state: params[:state])

    render json: @forest.name.to_s
  end

  def destroy
    @forest = find_forest_by_id
    @forest.destroy

    render json: @forest.name.to_s
  end

  private

  def find_forest_by_id
    Forest.find(params[:id])
  end
end
