class TrailsController < ApplicationController
  def index
    @trails = Trail.all

    render json: @trails
  end

  def show
    @trail = find_trail_by_id

    render json: @trail
  end

  def create
    @trail = Trail.create(trail_params)

    render json: @trail
  end

  def update
    @trail = find_trail_by_id
    @trail.update(trail_params)

    render json: @trail.name.to_s
  end

  def destroy
    @trail = find_trail_by_id
    @trail.destroy

    render json: @trail.name.to_s
  end

  private

  def find_trail_by_id
    Trail.find(params[:id])
  end

  def trail_params
    params.require(:trail).permit(:name, :miles, :forest_id)
  end
end
